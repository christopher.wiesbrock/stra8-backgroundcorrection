# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 10:48:53 2023

@author: wiesbrock
"""

import glob
import numpy as np
import pandas as pd
import os
from tqdm import tqdm

path=r'C:\Users\wiesbrock\Desktop\stra8 loca copy\*'
folders=glob.glob(path)

for i in range(len(folders)):
    file_path=folders[i]+'\ROIs_Results_MaxAverage_RedSub_FG\*'
    files=glob.glob(file_path)
    list_gre=[]
    list_red=[]
    for k in files:
        if "DeepGre" in k:
            list_gre.append(k)
            
    for k in files:
        if "DeepRed" in k:
            list_red.append(k)
            
    for n in tqdm(range(len(list_gre))):
        green_data=pd.read_csv(list_gre[n])
        red_data=pd.read_csv(list_red[n])
        corrected_data=red_data.copy()
        names=red_data.columns
        names=names[1:]
        
        for j in names:
            factor=np.mean(green_data[j])-np.mean(red_data[j])
            corrected_data[j]=green_data[j]-(red_data[j]+factor)
        output=list_gre[n][:-15]+'tif_DeepCor.csv'    
        corrected_data.to_csv(output)
